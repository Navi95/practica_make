#include <stdio.h>
#include <point.h>
#include <math.h>

int main()
{
	float x;
	float y;
	float z;
	float result;
	

	printf("Ingrese las coordenadas del punto #1\n");
	printf("x: ");
	scanf("%f", &x);
	printf("\ny: ");	
	scanf("%f", &y);
	printf("\nz: ");
	scanf("%f", &z);
	Point p1={.x=x,.y=y,.z=z};
	
	printf("\nIngrese las coordenadas del punto #2\n");
	printf("x: ");
	scanf("%f", &x);
	printf("\ny: ");	
	scanf("%f", &y);
	printf("\nz: ");
	scanf("%f", &z);
	Point p2={.x=x,.y=y,.z=z};
	
	result= distancia_euclidiana(p1,p2);

	printf("\n\nLa distancia euclidiana entre los puntos es: %.3f\n", result);
		
			
	
}
