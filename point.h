#include <stdlib.h>
#include <math.h>

//tipo de dato Point
typedef struct{
	float x,y,z;
	
}Point;

// declaracion de funcion distancia euclidiana
float distancia_euclidiana(Point a, Point b);
