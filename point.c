#include <stdio.h>

#include <stdlib.h>

#include <math.h>

#include <point.h>



float distancia_euclidiana(Point a, Point b){

	float disX,disY,disZ;
	disX = b.x - a.x;

	disY = b.y - a.y;

	disZ = b.z - a.z;
	float distancia;

	float sumaCuadrados = pow(disX, 2) + pow(disY, 2) + pow(disZ, 2);
	distancia = sqrt( sumaCuadrados );


	return distancia;

}
