CC=gcc
CFLAGS=-lm -I.

%.o: %.c point.h
	$(CC) -c -o $@ $< $(CFLAGS)

point: main.o point.o
	$(CC) -o $@ $^ $(CFLAGS)

clean: 
	rm  -f *.o point

